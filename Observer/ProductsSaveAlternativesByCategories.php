<?php

namespace Bitbull\AlternativesByCategory\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Api\Data\ProductLinkInterfaceFactory;

class ProductsSaveAlternativesByCategories implements ObserverInterface
{
    public $_request;
    protected $_productCollectionFactory;
    protected $_categoryFactory;
    protected $_productLinkInterfaceFactory;

    public function __construct(
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Api\Data\ProductLinkInterfaceFactory $_productLinkInterfaceFactory

    ) {
        $this->_request = $request;
        $this->_categoryFactory = $categoryFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_productLinkInterfaceFactory = $_productLinkInterfaceFactory;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $_product = $observer->getProduct();
        $productRequest= $this->_request->getParam('product');
        $productOrigData = $_product->getOrigData('alternatives_categories');
        if(!$productOrigData){
            $productOrigData = array();
        }elseif (!is_array($productOrigData)){
            $productOrigData = explode(',', $productOrigData);
        }
        if($productRequest &&
            ($this->_request->getParam('id') == $_product->getId() ) ){
            $productRequestCategoryIds = (isset($productRequest['alternatives_categories']))?$productRequest['alternatives_categories']:false;
            if($productRequestCategoryIds !== false
                && (array_diff($productRequestCategoryIds,$productOrigData) || array_diff($productOrigData,$productRequestCategoryIds))
            ){
                $position = 0;
                $linkDataAll = [];
                $arrayLinks = "" ;
                $productLinkInterface =  $this->_productLinkInterfaceFactory->create();
                $productLinkInterface->setLinkType(\Magento\Catalog\Model\Product\Link::LINK_TYPE_UPSELL);
                foreach ($productRequestCategoryIds as $categoryId){

                    $category = $this->_categoryFactory->create()->load($categoryId);
                    $collection = $this->_productCollectionFactory->create();
                    $collection->addAttributeToSelect('sku');
                    $collection->addCategoryFilter($category);
                    $collection->addAttributeToFilter('visibility', \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH);
                    $collection->addAttributeToFilter('status',\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);

                    foreach ($collection as $categoryProduct ){
                        $arrayLinks[$categoryProduct->getId()] = array('position' => $position++);
                       // $linkDataAll[] =  $productLinkInterface->setLinkedProductSku($categoryProduct->getSku());
                    }
                }

                if($arrayLinks) {
                    $_product->setUpSellLinkData($arrayLinks);
                    //$arrayLinks['upsell'] = (implode('='.base64_encode('position').'&',$arrayLinks)).'='.base64_encode('position');
                    //$newParams = array_merge($this->_request->getParams(),array('links' => $arrayLinks));
                    //$this->_request->setParams($newParams);
                   // $_product->setProductLinks($linkDataAll);
                }

            }
        }



    }
}
