<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

// @codingStandardsIgnoreFile

namespace Bitbull\AlternativesByCategory\Block\ProductList;


class Upsell extends \Magento\Catalog\Block\Product\ProductList\Upsell
{

    /**
     * @return $this
     */
    protected function _prepareData()
    {
        $product = $this->_coreRegistry->registry('product');
        /* @var $product \Magento\Catalog\Model\Product */
        if(!$product->getOrderUpsell()){
            return parent::_prepareData();
        }
        switch($product->getOrderUpsell()){

            case 1:
                $this->_itemCollection = $product->getUpSellProductCollection()->setPositionOrder('DESC')->addStoreFilter();
            case 2:
                $this->_itemCollection = $product->getUpSellProductCollection()->setOrder('news_from_date','DESC')->addStoreFilter();
            case 3:
                $this->_itemCollection = $product->getUpSellProductCollection()->setOrder('base_price')->addStoreFilter();
            default:
            $this->_itemCollection = $product->getUpSellProductCollection()->setPositionOrder()->addStoreFilter();
        }

        if ($this->moduleManager->isEnabled('Magento_Checkout')) {
            $this->_addProductAttributesAndPrices($this->_itemCollection);
        }
        $this->_itemCollection->setVisibility($this->_catalogProductVisibility->getVisibleInCatalogIds());

        $this->_itemCollection->load();

        /**
         * Updating collection with desired items
         */
        $this->_eventManager->dispatch(
            'catalog_product_upsell',
            ['product' => $product, 'collection' => $this->_itemCollection, 'limit' => null]
        );

        foreach ($this->_itemCollection as $product) {
            $product->setDoNotUseCategoryId(true);
        }

        return $this;
    }

}
