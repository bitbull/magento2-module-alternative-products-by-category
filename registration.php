<?php

use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE,
    'Bitbull_AlternativesByCategory',
    __DIR__
);
